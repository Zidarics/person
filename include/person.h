/*
 * person.h
 *
 *  Created on: Feb 24, 2020
 *      Author: zamek
 */

#ifndef PERSON_H_
#define PERSON_H_

#include <sys/queue.h>

#define STRICT

enum PersonState {
	SINGLE=0,
	MARRIED=1,
	DIVORCED=2,
	WIDOW=3
};

enum Ordering {
	ASCENDING,
	DESCENDING
};

struct Person {
	char *name;
	int age;
	enum PersonState state;
	char *died;
	TAILQ_ENTRY(Person) next;
};

int person_init();

int person_deinit();

int person_load_from_file(const char *name);

int person_add(struct Person *person);

struct Person *person_remove_head();

char *person_get_state(const struct Person *person);

void person_print_a_person(const struct Person *person);

void person_print_all();

struct Person *person_find_by_name(const char *name, struct Person *from);

struct Person *person_find_by_age(int age, struct Person *from);

struct Person *person_first();

struct Person *person_last();

int compare_by_name(struct Person *a, struct Person *b);

void person_bubble_sort(enum Ordering ordering,
						int (* comparer)(struct Person *a, struct Person *b));





#endif /* PERSON_H_ */
