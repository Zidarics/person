#Make for person application

BIN=bin
SRC=src
CFLAGS=-Wall -g -pedantic --std=c99

$(BIN)/person: $(BIN) $(BIN)/person.o 
		$(CC) -o $(BIN)/person $(BIN)/person.o $(CFLAGS)
		
$(BIN)/%.o:$(SRC)/%.c
		$(CC) -c $< -o $@ $(CFLAGS)    		
		
$(BIN):
		mkdir $(BIN)
				
clean:
		$(RM) -r $(BIN)
		
all: $(BIN)/person

.PHONY: clean all

						